# Lappky



[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)


### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ cd APILappky
$ npm install
```

For development environments...

```sh
$ npm start
```

### Users registered
-------------------
#

| Username | Password | 
| ------ | ------ | 
| angryduck640 | harcore | 
| orangesnake420 | stunner | 
| infantry | lxcOq8tw | 
| bigkoala465 | grateful | 
| orangesnake311 | bunker | 
| orangekoala125 | brown1 | 


# DEMO