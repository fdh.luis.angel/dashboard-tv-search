import users from '../database/users.json';



export function login(username, password){
    let loggedUser = undefined;

    users.forEach(user => {
        if(user.login.username === username && user.login.password === password){
            loggedUser = user
        }
    });

    saveOnLocalStorege(loggedUser);
    return loggedUser;
}


export function saveOnLocalStorege(loggedUser){
    let localStorage = window.localStorage;
    let user = {...loggedUser};
    //user.login.password = "";
    localStorage.setItem('user', JSON.stringify(user));
}

export function loadFromLocalStorage(){
    let localStorage = window.localStorage;
    let info = localStorage.getItem('user');
    return JSON.parse(info);
}

export function deleteFromLocalStorage(name){
    let localStorage = window.localStorage;
    localStorage.removeItem(name);
}