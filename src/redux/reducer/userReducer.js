import {types} from '../actions/';

const initState = {}

function reducer( state = initState, action ) {
    
    switch ( action.type ) {
        case types.SET_USER:
            let user = action.payload;
            return {...state, ...user};
        case types.DELETE_USER:
            return {};
        default:
            return state;
    }
}

export default reducer;
