import {types} from '../actions/';

const initState = []

function reducer( state = initState, action ) {
    
    switch ( action.type ) {
        case types.SET_SHOWS:
            return action.payload;
        default:
            return state;
    }
}

export default reducer;