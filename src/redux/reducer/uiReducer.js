import {types} from '../actions/';

const initState = {
    main_loading: false,
    alert:{
        show:false,
        message:"Alerta ejemplo",
        onClick:null,
        btnMessage:"UNDO"
    },
    sideBar:{
        
    }
}

function reducer( state = initState, action ) {
    
    switch ( action.type ) {
        case types.SET_MAIN_LOADING:
            return {
                ...state,
                main_loading: action.payload
            }
        case types.SET_ALERT:
            return {
                ...state,
                 alert: action.payload
            }
        default:
            return state;
    }
}

export default reducer;
