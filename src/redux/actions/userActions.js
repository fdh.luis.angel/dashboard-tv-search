import {types} from './';

export const setUser = ( loggedUser ) => ({
    type: types.SET_USER,
    payload: loggedUser
})

export const deleteUser = () => ({
    type: types.DELETE_USER
})