import {types} from './';

export const setMainLoading = ( loading ) => ({
    type: types.SET_MAIN_LOADING,
    payload: loading
})

export const setAlert = ( show, message, onClick, btnMessage ) => ({
    type: types.SET_ALERT,
    payload: {
        show, 
        message, 
        onClick,
        btnMessage
    }
})


export const setStyle = ( style ) => ({
    type: types.SET_ALERT,
    payload: {
        style
    }
})
