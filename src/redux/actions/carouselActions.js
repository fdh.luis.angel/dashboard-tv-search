import {types} from './';
import {URL_BASE} from '../../utils/urls';

export const setCarouselShows = ( shows) => ({
    type: types.CAROUSEL_SHOWS,
    payload: shows
})


export const startLoadingCarouselShows = (query) => {
    return async ( dispatch ) => {
        fetch(`${URL_BASE}${query}`).then(res => {
            if(res.ok)return res.json();
        }).then(data => {
            dispatch( setCarouselShows( data ) );
        });
    }
}