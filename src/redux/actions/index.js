export const types = {
    // UI
    SET_MAIN_LOADING : "[ui] main loading",
    SET_ALERT : "[ui] set alert",

    //User
    SET_USER : "[user] set user",
    DELETE_USER : "[user] delete user",

    //shows
    SET_SHOWS: "[show] set shows",
    ADD_SHOW: "[show] add show",
    
    CAROUSEL_SHOWS : "[carousel] set shows",

    SB_SET_STYLE : "[SideBar] set style",
}
