import {types} from './';
import {URL_BASE} from '../../utils/urls';
import {setMainLoading} from './uiActions'

export const setShows = ( shows) => ({
    type: types.SET_SHOWS,
    payload: shows
})


export const startLoadingShows = (query) => {
    return async ( dispatch ) => {
        dispatch(setMainLoading(true))
        fetch(`${URL_BASE}${query}`).then(res => {
            if(res.ok)return res.json();
        }).then(data => {
            dispatch( setShows( data ) );
            setTimeout(()=>{
                dispatch(setMainLoading(false));
            }, 2000);
        });
    }
 }
 

