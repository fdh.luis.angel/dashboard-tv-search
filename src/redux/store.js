import {
    createStore,
    combineReducers,
    applyMiddleware,
    compose
}
from 'redux'
import thunk from "redux-thunk";

//Reducers
import uiReducer from "./reducer/uiReducer";
import userReducer from './reducer/userReducer';
import showReducer from './reducer/showReducer';
import carouselReducer from './reducer/carouselReducer';

const composeEnhancers = (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
    ui: uiReducer,
    user: userReducer,
    shows : showReducer,
    carousel : carouselReducer
})

export default createStore(
    reducers,
    composeEnhancers(
        applyMiddleware(thunk)
    )
);
