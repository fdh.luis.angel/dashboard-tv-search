import React,{useEffect} from 'react';
import Swal from 'sweetalert2';
import {login} from '../utils/utils';
import {useDispatch} from 'react-redux';
import {setUser} from '../redux/actions/userActions';
import {Redirect} from 'react-router-dom';
import {loadFromLocalStorage} from '../utils/utils';

const style = {
    borderRadius:10,
    backgroundColor: "",
    color: "white",
    height: 100,
    top:-10,
    left: -10,
    padding: 50,
    marginBottom: 50
}


export default function Login() {

    const dispatch = useDispatch();

    const [username, setUsername] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [localUser, setLocalUser] = React.useState(undefined);

    useEffect(() => {
        let user = loadFromLocalStorage();
        
        if(user && user.login){
            dispatch(setUser(user));
            setLocalUser(user);
        }
    }, [dispatch])

    const handleUsername = (event) => {
        setUsername(event.target.value)
    }

    const handlePassword = (event) => {
        setPassword(event.target.value)
    }

    const handleLogin = () => {
        if(username.length > 0 && password.length>0){
            let loggedUser = login(username, password);
            if(!loggedUser){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Algo salió mal!',
                    footer: 'Error al iniciar sesión con: ' + username + " verifique que se encuentra registrado y la contraseña sea correcta"
                  })
            }else {
                dispatch(setUser(loggedUser));
                setLocalUser(loggedUser);
            }
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo salió mal!',
                footer: 'Clave y contraseña son requeridos'
              })
        }
    }
    

    return (
        <div className="container p-4">
            <div className="row">
                <div className="col"/>
                <div className="col-10 col-md-5 bg-light mt-5 p-5 card">
                    <div className="bg-dark" style={style}>
                        LOGIN
                    </div>
                    <div className="mt-2">
                        <div className="row">
                            <div className="col-sm-4 mt-2">Usuario</div>
                            <div className="col-12 col-sm">
                                <input type="text" className="form-control mb-3" placeholder="usuario" onChange={handleUsername}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-sm-4 mt-2">Password</div>
                            <div className="col-12 col-sm">
                                <input type="password" className="form-control " placeholder="password" onChange={handlePassword}/>
                            </div>
                        </div> 

                        <div className="row mt-3">
                            <div className="col-12 col-md mt-4">
                                <div className="btn btn-dark" onClick={handleLogin}>Iniciar sesión</div>
                            </div>
                        </div>                     
                    </div>
                </div>
                <div className="col"/>
            </div>
            {
                (localUser && localUser.login)?<Redirect to="/" /> :null
            }
        </div>
    )
}
