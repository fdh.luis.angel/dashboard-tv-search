import React, {useEffect} from 'react'
import FloatButton from '../components/FloatButton/FloatButton';
import Sidebar from '../components/SideBar/SideBar';
import NavBar from '../components/NavBar/NavBar';
import UserItems from '../components/UserItems/UserItems';
import ShowList from '../components/ShowList/ShowList';
import Carousel from '../components/Carousel/Carousel';
import {startLoadingShows} from '../redux/actions/showActions';
import {startLoadingCarouselShows} from '../redux/actions/carouselActions';
import {useDispatch, useSelector} from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';


export default function Main() {

    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();
    const carouselShows = useSelector(state => state.carousel);
    const {main_loading} = useSelector(state => state.ui);

    useEffect(() => {
        dispatch(startLoadingShows("american"));
        dispatch(startLoadingCarouselShows("space"));
    }, [dispatch])

    return (
            <div>
                {
                    main_loading ? <LinearProgress color="secondary"/>:null
                }
                
                <div className="row p-0 m-0">

                    <Sidebar open={open} setOpen={setOpen}/>
                    
                    <div className="col"> 
                        
                        <NavBar/>
                        
                        <Carousel shows={carouselShows}/>

                        <UserItems/>

                        <div className="bg-dark m-2 p-2">

                            <ShowList/>
                            
                        </div>
                    </div>
                </div>
                <FloatButton open={open} setOpen={setOpen}/>
            </div>
    )
}
