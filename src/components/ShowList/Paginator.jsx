import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';


const useStyles1 = makeStyles((theme) => ({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
      marginTop:'15px',
      color: "#FFF"
    },
    title: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
      display: 'inline-block',
      margin:'5px',
      marginRight:'20px'
    },
    buttons: {
      display: 'inline-block'
    }
  }));


export default function Paginator(props) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage, rowsPerPageChange } = props;

    const handleChangeRowsPerPage = (event) => {
        rowsPerPageChange(event.target.value);
    };

    const handleFirstPageButtonClick = (event) => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = (event) => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };


    return (
        <div className="bg-light">

            <Grid container justify="flex-end">

                <Grid item lg>
                    <Typography
                        className={classes.title}
                    >{`Rows per page:`}
                    </Typography>


                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={rowsPerPage}
                        onChange={handleChangeRowsPerPage}
                    >

                        <MenuItem value={6}>6</MenuItem>
                        <MenuItem value={10}>10</MenuItem>
                        <MenuItem value={15}>15</MenuItem>
                        <MenuItem value={count}>Todos</MenuItem>
                    </Select>


                    <Typography
                        className={classes.title}
                    >{((page * rowsPerPage + rowsPerPage) > count) ?
                        `${page * rowsPerPage}-${count} of ${count}`
                        : `${page * rowsPerPage}-${page * rowsPerPage + rowsPerPage} of ${count}`}
                    </Typography>

                    <div className={classes.buttons}>
                        <IconButton
                            onClick={handleFirstPageButtonClick}
                            disabled={page === 0}
                            aria-label="first page"
                        >
                            {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                        </IconButton>
                        <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                        </IconButton>
                        <IconButton
                            onClick={handleNextButtonClick}
                            disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                            aria-label="next page"
                        >
                            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                        </IconButton>
                        <IconButton
                            onClick={handleLastPageButtonClick}
                            disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                            aria-label="last page"
                        >
                            {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                        </IconButton>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}
