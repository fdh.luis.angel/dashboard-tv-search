import React from 'react';
import CardShow from '../CardShow/CardShow';
import Paginator from './Paginator';
import {useSelector} from 'react-redux'


export default function ShowList() {

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(6);

    const shows = useSelector(state => state.shows);

    //Paginator 
    const onChangePage = (event, newPage) =>{
        setPage(newPage);
    }

    const onChangeRowsPerPage = (value) =>{
        setRowsPerPage(value);
    }

    return (
        <div className="row">
            {
                shows && 
                (rowsPerPage > 0
                    ? shows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    : shows
                  ).map((show, i) => <CardShow tvShow={show} key={i}/>)
            }
            
            
            <Paginator
                rowsPerPageOptions={[6, 10, 20, { label: 'All', value: -1 }]}
                count={shows.length}
                rowsPerPage={rowsPerPage}
                rowsPerPageChange = {onChangeRowsPerPage}        
                onChangePage={onChangePage}
                page={page}
            />
        </div>
    )
}
