import React from 'react'
import './CardShow.css'
import image from './image_404.png';
import CardActionArea from '@material-ui/core/CardActionArea';


export default function CardShow({tvShow}) {

	const handleClick = () => {
		console.log("click")
	}

    return (
        <div className="cardShow col-12 col-sm-6 col-xl-3 col-xxl-2 p-0">
			{tvShow ?
					<div className="cardContainer">
						<CardActionArea onClick={handleClick}>
							<img  className="cardImage" src= {tvShow.show.image?tvShow.show.image.medium:image} alt=""/>
							<h5 className="p-2">{tvShow.show.name}</h5>  
							<p >{tvShow.show.type}</p> 
						</CardActionArea>     
					</div>
			:null}
        </div>
    )
}
