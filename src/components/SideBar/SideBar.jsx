import React from 'react'
import './SideBar.css'
import {startLoadingShows} from '../../redux/actions/showActions';
import {useDispatch} from 'react-redux'


export default function SideBar({open, setOpen}) {

    const [position, setPosition] = React.useState("relative");
    const dispatch = useDispatch();

    const [style, setStyle] = React.useState({
        backgroundColor: "rgb(49, 49, 49)",
        top: 0,
        bottom: 0,
        position: "relative",
        color:"white",
        zIndex:3,
        width:"15em",
    });

    function handleResize() {
        if(window.innerWidth >= 780){
            setPosition("relative");
            setStyle({...style, position: "relative", left: "0em"})
        }
        else{
          setPosition("fixed");
          setStyle({...style, position: "fixed", 
                      left:open?"0em":"-15em",
                      float: "center"})
        }
        setOpen(false)
    }

    React.useEffect(() => {
        window.addEventListener('load', handleResize);
        window.addEventListener('resize', handleResize);

        return _ => {
            window.removeEventListener('resize', handleResize);
            window.removeEventListener('load', handleResize);
        }
    })

    const handleClose = () => {
        setPosition("fixed");
        setStyle({...style, left: open?"0em":"-15em", position: "fixed"})
        setOpen(false);
    }

    const handleButtons = (e) => {
        dispatch(startLoadingShows(e.target.id))
    }

    return (
        <div className="col-sm-2"
            style={{...style, 
                    left: open?"0em":position==="fixed"?"-15em": "0em", 
                    transition:position==="fixed"?"left .5s":""}
            }>
            
            <div className="close_btn" onClick={handleClose}>X</div>

            <div className="mt-5">
                <div className="option" onClick={handleButtons} id="terror">Terror</div>
                <div className="option" onClick={handleButtons} id="fantasy">Fantasy</div>
                <div className="option" onClick={handleButtons} id="action">Action</div>
                <div className="option" onClick={handleButtons} id="comedy">Comedy</div>
            </div>

        </div>
    )
}


