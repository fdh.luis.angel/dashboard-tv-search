import React from 'react';
import './Carousel.css'


export default function Carousel({shows}) {
    const defaultImage = "https://comojuega.files.wordpress.com/2013/11/hd-desktop-wallpaper-hd-dark-black-wallpapers-dark-black-wallpaper-dark-background-dark-wallpaper-23-1-1600x10001.jpg?w=1024";
    const notFoundImg = "https://i.pinimg.com/originals/24/58/5f/24585fc9b7433a224a6ff5506e346969.png";
    return (
        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
            <div className="carousel-inner">
                {
                    shows && shows.map((show, i) => {
                        return <div className={`carousel-item ${i === 0 ? "active" : ""}`} key={i}>
                            <div className="container_image_carousel">
                                <img src={show?.show?.image?.original || show?.show?.image?.medium || defaultImage} className="d-block w-100 image_carousel" alt="..." />
                            </div>
                            <div>
                                <img className="d-none d-xl-block float_image" src={show?.show?.image?.medium || notFoundImg} alt=""/>
                            </div>
                            <div className="carousel-caption caption d-none d-lg-block">
                                <h5>{show.show.name}</h5>
                                <p>{show.show.type}</p>
                            </div>
                        </div>
                    })
                }
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    )
}
