import React from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import './FloatButton.css'

export default function FloatButton({open, setOpen}) {
    return (
        <div className="float_button d-block d-md-none" onClick={setOpen}>
            <MenuIcon className="m-1"/>
        </div>
    )
}
