import React, {useEffect, useState} from 'react'
import "./NavBar.css";
import {useSelector, useDispatch} from 'react-redux';
import {deleteUser} from '../../redux/actions/userActions';
import {startLoadingShows} from '../../redux/actions/showActions'
import {deleteFromLocalStorage} from '../../utils/utils';


export default function NavBar() {

    const user = useSelector(status => status.user);
    const [image, setImage] = useState("https://cdn.aarp.net/content/dam/aarp/entertainment/movies-for-grownups/2020/07/1140-will-smith-esp.imgcache.rev.web.900.513.jpg");
    const [query, setQuery] = useState("");
    
    const dispatch = useDispatch();


    useEffect(() => {
        if(user){
            setImage(user.picture.medium);
        }
    }, [setImage, user])


    const handleLogout = () => {
        deleteFromLocalStorage('user');
        dispatch(deleteUser())
    }

    const handleQuery = (e) => {
        setQuery(e.target.value);
    }

    const handleKeyDown = (e) => {
        if(e.code === "Enter" || e.code === "NumpadEnter"){
            dispatch(startLoadingShows(query));
        }
    }

    

    return (
        <nav className="navbar navbar-light bg-dark">
            <div className="container-fluid">
                <input className="ms-5  ms-md-0 m-2 form-control w-auto"
                    type="search"
                    placeholder="Search"
                    aria-label="Search" 
                    onChange={handleQuery}
                    onKeyDown={handleKeyDown}/>

                    <div className="avatar dropstart">
                        <div className="container_circle_img dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <img className='crop'
                                src={image}
                                alt="User" />
                        </div>
                        <ul className="dropdown-menu mt-2" >
                            <li><div className="dropdown-item" onClick={handleLogout}>Logout</div></li>
                        </ul>
                    </div>
            </div>
        </nav>
    )
}
