import './App.css';
import  Main from './pages/Main';
import LoginPage from './pages/Login';
import {useSelector } from 'react-redux';
import DetailsPage from './pages/Details';

import {
  BrowserRouter as Router,
  Switch,
  Route, 
  Redirect
} from "react-router-dom";

function App() {

  const user = useSelector ( state => state.user);

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/login"  component={LoginPage}/>
          <Route path="/details"  component={DetailsPage}/>
          <Route path="/">
            {!user.login ? <Redirect to="/login" /> : <Main/>}
          </Route>
        </Switch>
      </div>
    </Router>    
  );
}

export default App;
